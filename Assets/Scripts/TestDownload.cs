﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class TestDownload : MonoBehaviour
{
    [SerializeField] private SpriteRenderer _sprite;
    [SerializeField] private string _url;

    public void DownloadImage()
    {
        StartCoroutine(InitAllCardVariety());
    }
    
    private IEnumerator InitAllCardVariety()
    {
        UnityWebRequest request = UnityWebRequestTexture.GetTexture(_url);

        yield return request.SendWebRequest();
        
        if (request.isNetworkError || request.isHttpError)
        {
            Debug.LogError(request.error);
        }
        else
        {
            Debug.Log("Error: " + request.error + "\n"
                      + "IsDone: " + request.isDone + "\n"
                      + "Progress: " + request.downloadProgress);
            Texture2D image = ((DownloadHandlerTexture) request.downloadHandler).texture;
            //Texture2D image = DownloadHandlerTexture.GetContent(request);
            _sprite.sprite = Sprite.Create(image, new Rect(0f, 0f, image.width, image.height), new Vector2(0, 0));
        }
    }
}
