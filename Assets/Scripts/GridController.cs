﻿using System.Collections.Generic;
using UnityEngine;

public class GridController : MonoBehaviour
{
    [SerializeField] private Vector2 _cellSize;
    [SerializeField] private Vector2 _cellGap;
    
    private CardSpawner _cardSpawner;
    private Grid<Card> _gridCards;
    private Vector2 _offsetFromCenter;

    private List<Vector2Int> _freePosition;
    
    public void InitGrid(CardSpawner cardSpawner, Vector2Int size)
    {
        _gridCards = new Grid<Card>(size);
        _cardSpawner = cardSpawner;
        
        InitFreePosition(size);
        
        for (int i = 0; i < size.x; i++)
        {
            for (int j = 0; j < size.y; j++)
            {
                Card card = _cardSpawner.SpawnCard();
                Vector2Int gridPostion = GetRandomPosition();
                card.transform.localPosition = new Vector3(gridPostion.x * (_cellSize.x + _cellGap.x) + _offsetFromCenter.x, gridPostion.y * (_cellSize.y + _cellGap.y) + _offsetFromCenter.y, 0);
                _gridCards[gridPostion.x, gridPostion.y] = card;
            }
        }
    }

    public void InitFreePosition(Vector2Int size)
    {
        for (int i = 0; i < size.x; i++)
        {
            for (int j = 0; j < size.y; j++)
            {
                _freePosition.Add(new Vector2Int(i, j));
            }
        }
    }

    private void Awake()
    {
        _freePosition = new List<Vector2Int>();
        _offsetFromCenter = new Vector2(_cellSize.x / 2f, _cellSize.y / 2f);
    }

    private Vector2Int GetRandomPosition()
    {
        int randomNumber = Random.Range(0, _freePosition.Count);
        Vector2Int position = _freePosition[randomNumber];

        _freePosition.RemoveAt(randomNumber);

        return position;
    }
}