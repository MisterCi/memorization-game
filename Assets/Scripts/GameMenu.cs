﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class GameMenu : MonoBehaviour
{
    [SerializeField] private CardIconLoader _cardIconLoader;
    [SerializeField] private MemoryGame _memoryGame;
    [SerializeField] private CanvasGroup _gameMenuGroup;
    [SerializeField] private CanvasGroup _loadingGroup;
    [SerializeField] private Button _startButton;
    [SerializeField] private Slider _loadingBar;
    [SerializeField] private Text _record;

    private int _pairCollected;
    
    private void Awake()
    {
        _startButton.interactable = false;
    }

    private void OnEnable()
    {
        _memoryGame.OnGameStart += SetActiveGameMenu;
        _memoryGame.OnGameEnd += SetActiveGameMenu;
        _memoryGame.OnPairCollected += ChangeRecordText;
        _cardIconLoader.OnDownloadEnd += ActiveStartButton;
        _cardIconLoader.OnDownloadHappen += ChangeLoadingProgress;
    }

    private void OnDisable()
    {
        _memoryGame.OnGameStart -= SetActiveGameMenu;
        _memoryGame.OnGameEnd -= SetActiveGameMenu;
        _memoryGame.OnPairCollected -= ChangeRecordText;
        _cardIconLoader.OnDownloadEnd -= ActiveStartButton;
        _cardIconLoader.OnDownloadHappen -= ChangeLoadingProgress;
    }
    
    private void SetActiveGameMenu()
    {
        _gameMenuGroup.DOFade(_gameMenuGroup.alpha <= 0.1f ? 1f : 0f, 1f);
        _gameMenuGroup.interactable = !_gameMenuGroup.interactable;
    }

    private void ActiveStartButton()
    {
        _loadingGroup.DOFade(0f, 1f);
        _record.gameObject.SetActive(true);
        _startButton.interactable = true;
    }
    
    private void ChangeLoadingProgress(int _downloadedNumber, int _allNumber)
    {
        _loadingBar.DOValue((float) _downloadedNumber / _allNumber, 0.15f);
    }

    private void ChangeRecordText()
    {
        _pairCollected++;
        _record.text = _pairCollected.ToString();
    }
}
