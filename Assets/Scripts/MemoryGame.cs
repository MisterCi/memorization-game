﻿using System.Collections;
using UnityEngine;

public class MemoryGame : MonoBehaviour
{
    public event System.Action OnGameStart;
    public event System.Action OnPairCollected;
    public event System.Action OnGameEnd;
    
    [SerializeField] private Vector2Int _size;
    [SerializeField] private GridController _gridController;
    [SerializeField] private CardSpawner _cardSpawner;
    [SerializeField] private CardIconLoader _cardIconLoader;
    
    private Card _lastCard;

    private bool _canTap;
    private int _needMemory;
    
    public void StartGame()
    {
        _cardSpawner.SetCardsVarieties(_cardIconLoader.CardVarieties);
        _canTap = false;
        _needMemory = _size.x * _size.y;
        _cardSpawner.ClearCards();
        _gridController.InitGrid(_cardSpawner, _size);
        StartCoroutine(SwipeAllCards());
        OnGameStart?.Invoke();
    }
    
    private void OnEnable()
    {
        Card.OnCardTap += Card_OnCardTap;
    }

    private void OnDisable()
    {
        Card.OnCardTap -= Card_OnCardTap;
    }

    private void Start()
    {
        _cardIconLoader.DownloadAndParseJson();
    }
    
    private void Card_OnCardTap(Card card)
    {
        if (!_canTap)
            return;

        if (card == _lastCard)
            return;

        card.SwipeCard();
        
        if (_lastCard == null)
        {
            _lastCard = card;
        }
        else
        {
            StartCoroutine(CheckPair(card));
        }
    }

    private IEnumerator SwipeAllCards()
    {
        Card[] cards = _cardSpawner.Cards;
        
        foreach (Card card in cards)
        {
            card.SwipeCard();
        }
        
        yield return new WaitForSeconds(5f);
        
        foreach (Card card in cards)
        {
            card.SwipeCard();
        }

        yield return new WaitForSeconds(1f);
        
        _canTap = true;
    }
    
    private IEnumerator CheckPair(Card card)
    {
        _canTap = false;
        
        if (_lastCard.CompareType(card.CardType))
        {
            yield return new WaitForSeconds(1f);

            _lastCard.DestroyWithAnimation();
            card.DestroyWithAnimation();
                
            _needMemory -= 2;
            OnPairCollected?.Invoke();

            if (_needMemory == 0)
            {
                OnGameEnd?.Invoke();
            }
        }
        else
        {
            yield return new WaitForSeconds(1f);

            card.SwipeCard();
            _lastCard.SwipeCard();
        }
            
        _lastCard = null;
        _canTap = true;
    }
}