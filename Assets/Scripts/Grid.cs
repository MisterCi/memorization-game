﻿using System;
using UnityEngine;

public class Grid<T>
{
    public int Width => _width;
    public int Height => _height;

    private int _width;
    private int _height;

    private T[,] _grid;
    
    public Grid(int width, int height)
    {
        _width = width;
        _height = height;

        _grid = new T[width, height];
    }

    public Grid(Vector2Int size) : this(size.x, size.y) { }
    
    public T this[int x, int y]
    {
        get
        {
            ValidateCoordinate(x, y);
            
            return _grid[x, y];
        }
        
        set
        {
            ValidateCoordinate(x, y);

            _grid[x, y] = value;
        }
    }

    private void ValidateCoordinate(int x, int y)
    {
        if (x < 0 || x >= _width)
        {
            throw new ArgumentOutOfRangeException(nameof(x), "The first coordinate must be greater than 0 and less than the weight");
        }
        
        if (y < 0 || y >= _height)
        {
            throw new ArgumentOutOfRangeException(nameof(y), "The second coordinate must be greater than 0 and less than the height");
        }
    }
}
