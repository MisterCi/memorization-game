﻿using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class CardSpawner : MonoBehaviour
{
    public Card[] Cards => _list.ToArray();

    [SerializeField] private GameObject _cardPrefab;

    private List<Card> _list;
    private List<CardVariety> _cardsVarieties;

    private CardVariety _nextCardVariety;
    
    public Card SpawnCard()
    {
        GameObject cardObject = Instantiate(_cardPrefab, transform);
        Card card = cardObject.GetComponent<Card>(); 
        _list.Add(card);

        if (_nextCardVariety == null)
        {
            _nextCardVariety = GetRandomCardVariety();
            card.CardType = _nextCardVariety.cardType;
            card.Icon = _nextCardVariety.icon;
        }
        else
        {
            card.CardType = _nextCardVariety.cardType;
            card.Icon = _nextCardVariety.icon;
            _nextCardVariety = null;
        }

        return card;
    }

    public void SetCardsVarieties(List<CardVariety> cardVarieties)
    {
        _cardsVarieties = cardVarieties;
    }
    
    public void ClearCards()
    {
        _list.Clear();
    }

    private CardVariety GetRandomCardVariety()
    {
        return _cardsVarieties[Random.Range(0, _cardsVarieties.Count)];
    }
   
    private void OnValidate()
    {
        if (_cardPrefab.GetComponent<Card>() == null)
        {
            Debug.LogError("_cardPrefab have to have Card component");
        }
    }

    private void Awake()
    {
        _list = new List<Card>();
    }
}

public class CardVariety
{
    public CardType cardType;
    public Sprite icon;
}