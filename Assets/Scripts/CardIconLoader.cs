﻿using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Networking;

public class CardIconLoader : MonoBehaviour
{
    public System.Action<int, int> OnDownloadHappen;
    public System.Action OnDownloadEnd;
    public List<CardVariety> CardVarieties => _cardVarieties;

    [SerializeField] private string _jsonUrl;

    private List<CardVariety> _cardVarieties;
    private Root _deserializeJson;
    
    private void Awake()
    {
        _cardVarieties = new List<CardVariety>();
    }

    public void DownloadAndParseJson()
    {
        StartCoroutine(DownloadJSON());
    }
    
    private void ParseJson(string _jsonString)
    {
        _deserializeJson = JsonConvert.DeserializeObject<Root>(_jsonString);

        StartCoroutine(InitAllCardVariety());
    }

    private IEnumerator DownloadJSON()
    {
        UnityWebRequest request = UnityWebRequest.Get(_jsonUrl);

        yield return request.SendWebRequest();
        
        if (request.isNetworkError || request.isHttpError)
        {
            Debug.LogError(request.error);
        }
        else
        {
            Debug.Log("Json with png download!");

            ParseJson(request.downloadHandler.text.Remove(0, 1));
        }
    }
    
    private IEnumerator InitAllCardVariety()
    {
        foreach (CardsVariety _cardVarietyJson in _deserializeJson.cardsVarieties)
        {
            CardVariety _cardVariety = new CardVariety();
            _cardVariety.cardType = (CardType) _cardVarietyJson.cardType;
            
            UnityWebRequest request = UnityWebRequestTexture.GetTexture(_cardVarietyJson.url);

            yield return request.SendWebRequest();
            
            if (request.isNetworkError || request.isHttpError)
            {
                Debug.LogError(request.error);
            }
            else
            {
                Debug.Log(_cardVariety.cardType + " download!");
                Texture2D image = ((DownloadHandlerTexture) request.downloadHandler).texture;
                _cardVariety.icon = Sprite.Create(image, new Rect(0f, 0f, image.width, image.height), new Vector2(0.5f, 0.5f), 100f);
            }

            _cardVarieties.Add(_cardVariety);
            
            OnDownloadHappen?.Invoke(_cardVarieties.Count, _deserializeJson.cardsVarieties.Count);
        }

        OnDownloadEnd?.Invoke();
    }
}

public class Root    
{
    public List<CardsVariety> cardsVarieties { get; set; } 
}

public class CardsVariety    
{
    public int cardType { get; set; } 
    public string url { get; set; } 
}