﻿using DG.Tweening;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class Card : MonoBehaviour
{
    public static event System.Action<Card> OnCardTap;

    public CardType CardType { get; set; }

    public Sprite Icon
    {
        get => _icon.sprite;
        set => _icon.sprite = value;
    }

    [SerializeField] private GameObject _frontSide;
    [SerializeField] private GameObject _backSide;
    [SerializeField] private SpriteRenderer _icon;

    private readonly Vector3 _vectorY90 = new Vector3(0f, 90f, 0f);
    private Vector3 _vectorY270 = new Vector3(0f, 270f, 0f);
    private Transform _frontTransform;
    private Transform _backTransform;
    private Collider2D _collider;

    public bool CompareType(CardType cardType)
    {
        return CardType == cardType;
    }

    public void SwipeCard()
    {
        if (_frontSide.activeSelf)
        {
            DOTween.Sequence()
                .Append(_frontTransform.DOLocalRotate(_frontTransform.eulerAngles + _vectorY90, 0.5f))
                .AppendCallback(() =>
                {
                    _frontSide.SetActive(false);
                    _frontTransform.eulerAngles = _vectorY270;
                })
                .AppendCallback(() => _backSide.SetActive(true))
                .Append(_backTransform.DOLocalRotate(_backTransform.eulerAngles + _vectorY90, 0.5f));
        }
        else
        {
            DOTween.Sequence()
                .Append(_backTransform.DOLocalRotate(_backTransform.eulerAngles + _vectorY90, 0.5f))
                .AppendCallback(() =>
                {
                    _backSide.SetActive(false);
                    _backTransform.eulerAngles = _vectorY270;
                })
                .AppendCallback(() => _frontSide.SetActive(true))
                .Append(_frontTransform.DOLocalRotate(_frontTransform.eulerAngles + _vectorY90, 0.5f));
        }
    }

    public void DestroyWithAnimation()
    {
        _collider.enabled = false;
        transform.DOScale(Vector3.zero, 0.5f)
            .OnComplete(() => Destroy(gameObject));
    }

    private void Awake()
    {
        _collider = GetComponent<Collider2D>();
        _frontTransform = _frontSide.transform;
        _backTransform = _backSide.transform;
    }

    private void OnMouseDown()
    {
        OnCardTap?.Invoke(this);
    }
}

public enum CardType
{
    None      = 0,
    Airplane  = 1,
    Ball      = 2,
    Bear      = 3,
    Boxes     = 4,
    Doll      = 5,
    Horse     = 6,
    House     = 7,
    Rabbit    = 8,
    Ship      = 9,
    Tower     = 10
}